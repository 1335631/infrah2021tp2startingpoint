import os
import sys
import subprocess

def runCommandToAddUser(username):
	print("Adding user...")
	os.system("useradd "+(username))

def runCommandToRemoveUser(username):
	print("Deleting user...")
	os.system("deluser " + (username))

def readFileByLine(filename):
	content = []
	print("Searching throw files...")
	file = open(filename, "r")
	content=file
	return content
