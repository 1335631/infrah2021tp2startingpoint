docker stop ds
docker container rm ds
docker volume rm vol_tp2ND_dockershare
docker image rm docker-share
docker build -t docker-share -f ./project/docker/Dockerfile .
docker volume create --name vol_tp2ND_dockershare --opt device=$PWD --opt o=bind --opt type=none
docker run -d -p 5556:5555 --mount source=vol_tp2ND_dockershare,target=/mnt/app/ --name ds docker-share
docker exec -it ds /bin/bash
